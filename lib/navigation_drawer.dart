import 'package:obadi/about_screen.dart';
import 'package:obadi/profile_screen.dart';

import 'imports.dart';

class NavigationDrawerr extends StatelessWidget {
  const NavigationDrawerr({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {},
      builder: (context, state) {
        var cubit = AppCubit().get(context);
        String name = CacheHelper.getData(key: 'name') ?? '';
        return Drawer(
          backgroundColor: Colors.transparent,
          child: Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
                borderRadius: AppLocalizations.of(context)?.localeName == 'en'
                    ? const BorderRadius.only(
                        topRight: Radius.circular(25),
                        bottomRight: Radius.circular(25))
                    : const BorderRadius.only(
                        topLeft: Radius.circular(25),
                        bottomLeft: Radius.circular(25)),
                gradient: LinearGradient(
                    colors: [mainColor, const Color.fromARGB(255, 32, 32, 32)],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight)),
            child: Column(children: [
              DrawerHeader(
                padding: const EdgeInsetsDirectional.only(bottom: 25),
                margin: EdgeInsets.zero,
                child: SizedBox(
                    width: double.infinity,
                    height: double.infinity,
                    child: GestureDetector(
                      onTap: () {
                        popNavigation(context);
                        pushNavigation(context, const ProfileScreen());
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 8),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(50)),
                            child: Image.asset(
                              'assets/images/ic_launcher.png',
                              height: 47,
                              width: 47,
                            ),
                          ),
                          Text(
                            name,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                    )),
              ),
              const SizedBox(
                height: 20,
              ),
              ListView(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                children: [
                  buildDrawerItem(
                      title: '${AppLocalizations.of(context)?.previous_scan}',
                      onTap: () {
                        popNavigation(context);
                        cubit.getPreScans();
                        pushNavigation(context, const PreviousScreen());
                      },
                      icon: Icons.qr_code),
                  buildDrawerItem(
                      title: '${AppLocalizations.of(context)?.win_reports}',
                      onTap: () {
                        popNavigation(context);
                        cubit.getTransitions();
                        pushNavigation(context, const WinReportScreen());
                      },
                      icon: Icons.transit_enterexit_rounded),
                  buildDrawerItem(
                      title: '${AppLocalizations.of(context)?.contcat_us}',
                      onTap: () {
                        popNavigation(context);
                        cubit.luanchUri();
                      },
                      icon: Icons.call),
                  ExpansionTile(
                    title: Text(
                      '${AppLocalizations.of(context)?.language}',
                    ),
                    textColor: Colors.white,
                    collapsedTextColor: Colors.white,
                    collapsedIconColor: Colors.white,
                    iconColor: Colors.white,
                    children: [
                      RadioListTile(
                        title: const Text(
                          'English',
                          style: TextStyle(color: Colors.white),
                        ),
                        activeColor: Colors.white,
                        value: const Locale('en'),
                        groupValue: cubit.locale,
                        onChanged: (value) {
                          cubit.changeLocale('en');
                          popNavigation(context);
                        },
                      ),
                      RadioListTile(
                        title: const Text(
                          'العربية',
                          style: TextStyle(color: Colors.white),
                        ),
                        activeColor: Colors.white,
                        value: const Locale('ar'),
                        groupValue: cubit.locale,
                        onChanged: (value) {
                          cubit.changeLocale('ar');
                          popNavigation(context);
                        },
                      )
                    ],
                  ),
                  buildDrawerItem(
                      title: '${AppLocalizations.of(context)?.about_us}',
                      onTap: () {
                        popNavigation(context);
                        pushNavigation(context, const AboutScreen());
                      },
                      icon: Icons.info_outline),
                ],
              ),
              const Expanded(child: SizedBox()),
              GestureDetector(
                onTap: () {
                  navigateRemove(context, LoginScreen());
                  cubit.removeCache();
                },
                child: Container(
                  padding: const EdgeInsets.all(12),
                  decoration: BoxDecoration(color: mainColor),
                  child: Center(
                      child: Text(
                    '${AppLocalizations.of(context)?.logout}',
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w500),
                  )),
                ),
              )
            ]),
          ),
        );
      },
    );
  }
}
