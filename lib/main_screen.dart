import 'package:obadi/imports.dart';
import 'package:obadi/web_mobile_scanner.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {
        if (state is PostScanQrCodeErrorState) {
          if (state.statusCode == 400) {
            showSnackBarr(msg: "${AppLocalizations.of(context)?.alreadyTaken}");
          } else {
            showSnackBarr(
                msg: '${AppLocalizations.of(context)?.connection_failed}');
          }
        } else if (state is PostScanQrCodeSuccessState) {
          showWinDialoge(
            context,
          );
        } else if (state is StartTransitionSuccessState) {
          showSnackBarr(
              msg: "${AppLocalizations.of(context)?.transition_done}",
              color: Colors.green);
        } else if (state is StartTransaitionErrorState) {
          if (state.statusCode == 400 &&
              state.message == "User is on dept to syriatel") {
            showSnackBarr(msg: "الرجاء تسديد الدين قبل التحويل");
          } else {
            showSnackBarr(
                msg: "${AppLocalizations.of(context)?.connection_failed}");
          }
        }
      },
      builder: (context, state) {
        var cubit = AppCubit().get(context);

        double point = CacheHelper.getData(key: 'point') ?? 0.0;
        List<double> values = [point];
        double max = values[0] > 1000 ? values[0] + 5700 : values[0] + 570;
        LinearGradient colors = LinearGradient(
            colors: [mainColor, const Color.fromARGB(255, 34, 34, 34)]);
        return Scaffold(
            drawer: const NavigationDrawerr(),
            appBar: ObadiAppBar('${AppLocalizations.of(context)?.home}')
                .build(context),
            body: Center(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 12),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Column(
                        children: [
                          Text(
                            '${AppLocalizations.of(context)?.welcome_to_3obadi_application}',
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 18,
                                fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(height: 7),
                          Text('مع 3obadi ال QR بتحكي',
                              style: TextStyle(
                                  color: mainColor,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600),
                              textAlign: TextAlign.center,
                              textDirection: TextDirection.rtl),
                        ],
                      ),
                      // const SizedBox(
                      //   height: 75,
                      // ),
                      InkWell(
                        borderRadius: BorderRadius.circular(119),
                        onTap: () {
                          pushNavigation(context, const WebMobileScanner());
                        },
                        child: Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                              colors: [
                                mainColor,
                                const Color.fromARGB(255, 32, 32, 32)
                              ],
                            ),
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.black38,
                                offset: Offset(10.0, 10.0),
                                blurRadius: 31,
                                spreadRadius: 0.0,
                              ),
                            ],
                          ),
                          child: const Icon(
                            Icons.qr_code_scanner_outlined,
                            size: 80,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      // const SizedBox(
                      //   height: 50,
                      // ),
                      Column(
                        children: [
                          Text(
                            '${AppLocalizations.of(context)?.your_points_until_now}',
                            style: TextStyle(
                                color: mainColor,
                                fontSize: 20,
                                fontWeight: FontWeight.w700),
                          ),
                          Row(
                            children: [
                              Text(
                                '${point.round()} ${AppLocalizations.of(context)?.point}',
                                style: TextStyle(
                                    color: mainColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600),
                              ),
                              Expanded(
                                child: FlutterSlider(
                                  rtl: true,
                                  disabled: true,
                                  trackBar: FlutterSliderTrackBar(
                                      activeTrackBarHeight: 11,
                                      inactiveTrackBarHeight: 11,
                                      inactiveTrackBar: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      activeDisabledTrackBarColor: mainColor,
                                      activeTrackBar: BoxDecoration(
                                          gradient: colors,
                                          borderRadius:
                                              BorderRadius.circular(20))),
                                  tooltip: FlutterSliderTooltip(disabled: true),
                                  values: values,
                                  max: max,
                                  min: 0,
                                  handler: FlutterSliderHandler(
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                          boxShadow: const [
                                            BoxShadow(
                                                offset: Offset(0, 0),
                                                blurRadius: 6,
                                                color: Colors.black38)
                                          ]),
                                      child: Image.asset(
                                          'assets/images/ic_launcher.png')),
                                ),
                              ),
                            ],
                          ),
                          GestureDetector(
                            onTap: () {
                              showTransitionDialog(context, cubit, point);
                            },
                            child: Container(
                              width: 150,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  gradient: colors,
                                  borderRadius: BorderRadius.circular(25)),
                              child: state is StartTransaitionLoadingState
                                  ? const Center(
                                      child: CircularProgressIndicator(
                                          color: Colors.white),
                                    )
                                  : Center(
                                      child: Text(
                                          '${AppLocalizations.of(context)?.drink_and_talk}',
                                          style: const TextStyle(
                                              fontSize: 20,
                                              color: Colors.white,
                                              fontFamily: 'Pacifico')),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ]),
              ),
            ));
      },
    );
  }
}
