import 'dart:io';

import 'package:dio/dio.dart';
import 'package:obadi/imports.dart';

class DioHelper {
  static Dio? dio;

  static init() {
    dio = Dio(BaseOptions(
      baseUrl: 'https://obadi.sy/api/v1/',
      headers: {'Content-Type': 'application/json'},
    ));
  }

  static Future<Response?> getData(String path,
      {Map<String, dynamic>? queries, Map<String, dynamic>? headres}) async {
    return await dio?.get(path,
        queryParameters: queries, options: Options(headers: headres));
  }

  static Future<Response?> postData(String path, entries,
      {Map<String, dynamic>? queries, Map<String, dynamic>? headres}) async {
    return await dio?.post(path,
        data: entries,
        queryParameters: queries,
        options: Options(headers: headres));
  }

  static Future putData(String path, Map<String, dynamic> entries) async {
    await dio?.put(path, data: entries);
  }

  // static Future reqwithauthorization(
  //     String path, Map<String, dynamic>? data) async {
  //   String token = CacheHelper.getData(key: "token");
  //   Response? response = await dio?.post(path,
  //       data: data,
  //       options: Options(headers: {"Authorization": "Bearer $token"}));
  //   if (response?.statusCode == 401) {
  //     String mobile = CacheHelper.getData(key: "mobile");
  //     String password = CacheHelper.getData(key: "password");
  //     Response? getToken = await dio
  //         ?.post("auth/login", data: {"mobile": mobile, "password": password});
  //     String newToken = getToken?.data["token"];
  //     Response? origin = await dio?.post(path,
  //         options: Options(headers: {"Authorization": "Bearer $newToken"}),
  //         data: data);
  //     return origin;
  //   } else {
  //     return response;
  //   }
  // }
  static Future<Response?> sendAuthenticatedRequest(String url, Options options,
      {Map<String, dynamic>? data}) async {
    // Check if the JWT access token is available in the cache
    // String accessToken = CacheHelper.getData(key: "token");

    // // Set the access token in the request header
    // options.headers?["Authorization"] = "Bearer $accessToken";

    try {
      // Send the request with the existing access token
      return await dio?.request(url, data: data, options: options);
    } on DioError catch (error) {
      print(error.response?.data.toString());
      // Check if the response returned unauthorized (status code 401)
      if (error.response?.statusCode == 401) {
        // Retrieve the phone number and password from the cache
        String phoneNumber = CacheHelper.getData(key: "mobile");
        String password = CacheHelper.getData(key: "password");

        // Call the login API to get a new JWT access token
        try {
          // Make a POST request to the login API with the phone number and password
          Response? response = await dio?.post(
            "auth/login",
            data: {'mobile': phoneNumber, 'password': password},
          );

          // Assuming the response contains the new access token
          String newAccessToken = response?.data['token'];

          // Save the new access token to the cache for future use
          CacheHelper.setString(key: "token", value: newAccessToken);
          print(newAccessToken);

          // return newAccessToken;
        } catch (error) {
          CacheHelper.removeData();
          rethrow;
        }

        // Set the new access token in the request header
        options.headers?["Authorization"] =
            "Bearer ${CacheHelper.getData(key: "token")}";

        // Resend the request with the new access token
        return await dio?.request(url, options: options, data: data);
      } else {
        rethrow;
      }
    }
  }

  static Future<String?> loginAndGetAccessToken(
      String phoneNumber, String password) async {
    // Assuming you have a login API endpoint

    try {
      // Make a POST request to the login API with the phone number and password
      Response? response = await dio?.post(
        "auth/login",
        data: {'mobile': phoneNumber, 'password': password},
      );

      // Assuming the response contains the new access token
      String newAccessToken = response?.data['token'];

      // Save the new access token to the cache for future use
      CacheHelper.setString(key: "token", value: newAccessToken);

      return newAccessToken;
    } catch (error) {
      CacheHelper.removeData();
    }
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
