import 'imports.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ObadiAppBar('${AppLocalizations.of(context)?.about_us}')
          .build(context),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Card(
            elevation: 15,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset(
                      'assets/images/logo.png',
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      'تأسست شركة عُوبَدي منذ عام 2014 و منذ بدايتها إتبعت منهج فكري غير تقليدي بمفهوم خاص نواته تنادي بضفرة العلوم و الإستفادة من جميعها بما يخص الهدف المرجو الوصول إليه ضمن ضوابط فكرية و أسس علمية سليمة و لها مؤشر حساس متمثل ببوصلة تعتمد على الثبات من جهة و الطواف من جهة أخرى لإستخدام الإمكانيات المتاحة برؤية مستقبلية بشكل موضوعي مع مراعاة الواقع بشكل دقيق ليتترجم ذلك بترك بصمة في أي مجال أرادت أن تكون متواجدة فيه و الله وليُّ التوفيق',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                      textDirection: TextDirection.rtl,
                    )
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
