import 'imports.dart';

class CacheHelper {
  static SharedPreferences? sharedPreferences;

  static Future init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

  static setString({required String key, required String value}) async {
    await sharedPreferences?.setString(key, value);
  }

  static setBool({required String key, required bool value}) async {
    await sharedPreferences?.setBool(key, value);
  }

  static setDouble({required String key, required double value}) async {
    await sharedPreferences?.setDouble(key, value);
  }

  static getData({required String key}) {
    return sharedPreferences?.get(key);
  }

  static removeData() {
    sharedPreferences?.clear();
  }
}
