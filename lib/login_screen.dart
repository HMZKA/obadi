import 'package:obadi/new_password_screen.dart';
import 'package:phone_number/phone_number.dart';

import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {
  LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var formkey = GlobalKey<FormState>();
  bool? isSyrianNumber;
  Future validatePhoneNumber(value) async {
    await PhoneNumberUtil().validate("$value", regionCode: 'SY').then((value) {
      setState(() {
        isSyrianNumber = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AppLoginErrorState) {
          state.statusCode == 0
              ? showSnackBarr(
                  msg: "${AppLocalizations.of(context)?.connection_failed}")
              : showSnackBarr(
                  msg:
                      "${AppLocalizations.of(context)?.incorrect_credentials}");
        } else if (state is AppLoginSuccessState) {
          var cubit = AuthCubit().get(context);
          cubit.phoneController.text = "";
          cubit.loginpasswordController.text = "";
          state.isVerify
              ? navigateRemove(context, const MainScreen())
              : {
                  pushNavigation(context, OtpScreen()),
                  AuthCubit().get(context).requestCode()
                };
        }
      },
      builder: (context, state) {
        var cubit = AuthCubit().get(context);
        var code = CacheHelper.getData(key: 'code') ?? '';
        return Scaffold(
            appBar: ObadiAppBar('${AppLocalizations.of(context)?.login}')
                .build(context),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 15),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Form(
                  key: formkey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 140,
                        child: Image.asset(
                          'assets/images/logo.png',
                          width: MediaQuery.of(context).size.width / 1.5,
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      textBox(
                          AppLocalizations.of(context)?.phone_number,
                          (String? value) {
                            if (value!.isEmpty) {
                              return '${AppLocalizations.of(context)?.you_should_enter_a_phone_number}';
                            }
                            final firstnumber = value[0];
                            if (firstnumber == "0") {
                              cubit.phoneController.text =
                                  "963${value.substring(1)}";
                            }
                          },
                          cubit.phoneController,
                          TextInputType.phone,
                          onChange: (value) {
                            validatePhoneNumber(cubit.phoneController.text);
                          }),
                      const SizedBox(
                        height: 15,
                      ),
                      textBox(AppLocalizations.of(context)?.password,
                          (String? value) {
                        if (value!.isEmpty) {
                          return '${AppLocalizations.of(context)?.you_should_enter_password}';
                        }
                      }, cubit.loginpasswordController,
                          TextInputType.visiblePassword,
                          suffixIcon: IconButton(
                            icon: Icon(
                              cubit.icon,
                              color: mainColor,
                            ),
                            onPressed: () {
                              cubit.suffixicon();
                            },
                          ),
                          isScure: cubit.isScure),
                      const SizedBox(
                        height: 20,
                      ),
                      signButton(
                          context,
                          Center(
                              child: state is AppLoginLoadingState
                                  ? const CircularProgressIndicator(
                                      color: Colors.white,
                                    )
                                  : Text(
                                      '${AppLocalizations.of(context)?.login}',
                                      style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    )), () {
                        if (formkey.currentState!.validate()) {
                          cubit.login(cubit.phoneController.text,
                              cubit.loginpasswordController.text);
                        }
                      }, state),
                      const SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '${AppLocalizations.of(context)?.dont_have_account}',
                            style: const TextStyle(fontSize: 16),
                          ),
                          TextButton(
                            onPressed: () {
                              cubit.phoneController.text = '';
                              cubit.loginpasswordController.text = '';
                              pushNavigation(context, RegisterScreen());
                            },
                            style: ButtonStyle(
                                overlayColor: MaterialStateProperty.all(
                                    mainColor.withAlpha(100))),
                            child: Text(
                              '${AppLocalizations.of(context)?.create_account}',
                              style: TextStyle(color: mainColor),
                            ),
                          )
                        ],
                      ),
                      TextButton(
                        onPressed: () async {
                          if (cubit.phoneController.text.isEmpty) {
                            showSnackBarr(
                                msg:
                                    '${AppLocalizations.of(context)?.you_should_enter_a_phone_number}');
                          } else if (!isSyrianNumber!) {
                            showSnackBarr(
                                msg:
                                    "${AppLocalizations.of(context)?.syrianNumber}");
                          } else {
                            final firstnumber = cubit.phoneController.text[0];
                            if (firstnumber == "0") {
                              cubit.phoneController.text =
                                  "963${cubit.phoneController.text.substring(1)}";
                            }
                            cubit.requestResetCode(cubit.phoneController.text);
                            CacheHelper.setString(
                                key: "mobile",
                                value: cubit.phoneController.text);
                            pushNavigation(context, PasswordScreen());
                          }
                        },
                        style: ButtonStyle(
                            overlayColor: MaterialStateProperty.all(
                                mainColor.withAlpha(100))),
                        child: Text(
                          '${AppLocalizations.of(context)?.forget_password}',
                          style: TextStyle(color: mainColor),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ));
      },
    );
  }
}
