// import 'package:obadi/models/barcode_model.dart';
// import 'package:obadi/models/pre_scans_model.dart';
// import 'package:obadi/models/transitions_model.dart';
// import 'package:url_launcher/url_launcher.dart';

// import '../../imports.dart';
// import 'package:location/location.dart' as location;
// part 'app_state.dart';

// class AppCubit extends Cubit<AppState> {
//   AppCubit() : super(AppInitial());

//   AppCubit get(context) => BlocProvider.of(context);

//   QRViewController? qrViewController;
//   Barcode? result;

//   scanningData(p0) {
//     qrViewController = p0;
//     qrViewController?.scannedDataStream.listen((event) {
//       result = event;
//       postQrcode(result?.code);
//       emit(AppScanQrCodeState());
//     });
//   }

//   initCache() async {
//     await CacheHelper.init().then((value) {
//       CacheHelper.getData(key: 'locale');
//       locale = Locale(CacheHelper.getData(key: 'locale') ?? 'ar');
//       emit(AppInitCacheState());
//     });
//   }

//   String lang = CacheHelper.getData(key: 'locale') ?? 'ar';
//   Locale locale = Locale(CacheHelper.getData(key: 'locale') ?? 'ar');

//   changeLocale(String value) {
//     locale = Locale(value);
//     CacheHelper.setString(key: 'locale', value: value);

//     emit(AppChangeLocaleState());
//   }

//   askPermission() async {
//     await openAppSettings();
//     emit(AppAskPermissionState());
//   }

//   location.Location loc = location.Location();
//   dynamic permissionGranted;
//   bool? serviceEnabled;

//   Future checkPermission(id, context, cubit) async {
//     permissionGranted = await loc.hasPermission();
//     serviceEnabled = await loc.serviceEnabled();
//     startTransition(id, context, cubit);
//   }

//   String locate = '';
//   Future<String> enableService() async {
//     if (!serviceEnabled!) {
//       await loc.requestService().then((value) {
//         if (value) {
//           getLocation().then((value) {
//             locate = "${value.longitude},${value.latitude}";
//           });
//         } else {
//           showSnackBarr(msg: 'You should enable the location');
//         }
//       });
//     } else {
//       getLocation().then((value) {
//         locate = "${value.latitude},${value.longitude}";
//       });
//     }
//     return locate;
//   }

//   Future<location.LocationData> getLocation() async {
//     return await loc.getLocation().then((value) {
//       emit(AppGetLoctionState());
//       return value;
//     });
//   }

//   final Uri uri = Uri(scheme: 'tel', path: '+963969333060');
//   luanchUri() async {
//     await launchUrl(uri);
//     emit(AppLaunchUrlState());
//   }

// //----------------------------------------------------------------------------
//   PreScansModel? preScansModel;
//   getPreScans() {
//     emit(AppLoadingState());
//     DioHelper.getData('client/barcodes', headres: {
//       "Cookie": CacheHelper.getData(key: 'connect'),
//       "Set-Cookie": CacheHelper.getData(key: 'connect'),
//     }).then((value) {
//       preScansModel = PreScansModel.fromJson(value?.data);
//       emit(AppGetPreScansState());
//     }).catchError((error) {
//       print(error.toString());
//       emit(AppGetPreScansErrorState());
//     });
//   }

//   TransitionsModel? transitionsModel;
//   getTransitions() {
//     emit(AppLoadingState());
//     DioHelper.getData('client/transitions', headres: {
//       "Keep-Alive": "timeout=5, max=100",
//       "Cookie": CacheHelper.getData(key: 'connect'),
//       "Set-Cookie": CacheHelper.getData(key: 'connect'),
//     }).then((value) {
//       transitionsModel = TransitionsModel.fromJson(value?.data);
//       emit(AppGetTransitionsState());
//     }).catchError((error) {
//       emit(AppGetTransitionsErrorState());
//     });
//   }

//   startTransition(id, context, cubit) {
//     emit(AppLoadingState());
//     loc.requestPermission().then((value) async {
//       value.toString() == "PermissionStatus.denied" ||
//               value.toString() == "PermissionStatus.deniedForever"
//           ? openPermissionSettings(
//               context, cubit, AppLocalizations.of(context)?.access_location)
//           : await loc.requestService().then((value) {
//               if (value) {
//                 getLocation().then((value) {
//                   locate = "${value.latitude},${value.longitude}";
//                   DioHelper.postData('client/start/user/transition', {
//                     "amountTypeId": id,
//                     "location": locate
//                   }, headres: {
//                     "Cookie": CacheHelper.getData(key: 'connect'),
//                     "Set-Cookie": CacheHelper.getData(key: 'connect'),
//                   }).then((value) {
//                     getPoints();
//                     value?.data['result'] == 'success'
//                         ? showSnackBarr(
//                             msg: 'Done Successfully', color: Colors.green)
//                         : showSnackBarr(msg: 'Failed Try again');
//                     emit(AppTransaitionState());
//                   }).catchError((error) {
//                     showSnackBarr(msg: 'Failed Try again');
//                     emit(AppTransaitionState());
//                   });
//                 });
//               } else {
//                 showSnackBarr(msg: 'You should enable the location');
//               }
//             });
//     }).catchError((error) {});
//   }

//   BarcodeModel? barcodeModel;
//   postQrcode(result) {
//     emit(AppLoadingState());
//     DioHelper.postData('client/barcode/consume', {
//       "code": "$result"
//     }, headres: {
//       "Cookie": CacheHelper.getData(key: 'connect'),
//       "Set-Cookie": CacheHelper.getData(key: 'connect'),
//     }).then((value) {
//       barcodeModel = BarcodeModel.fromJson(value?.data);
//       startTransform(barcodeModel?.id);
//       emit(AppPostScanQrCodeState());
//     }).catchError((error) {
//       emit(AppPostScanQrCodeErrorState());
//     });
//   }

//   startTransform(code) {
//     emit(AppLoadingState());
//     DioHelper.postData('client/start/qr/transform', {
//       "barcodeId": code
//     }, headres: {
//       "Cookie": CacheHelper.getData(key: 'connect'),
//       "Set-Cookie": CacheHelper.getData(key: 'connect'),
//     }).then((value) {
//       getPoints();
//       emit(AppPostTransformsState(value?.data['result']));
//     }).catchError((error) {
//       emit(AppPostTransformsErrorState());
//     });
//   }

//   getPoints() {
//     emit(AppLoadingState());
//     DioHelper.getData('user/points', headres: {
//       "Cookie": CacheHelper.getData(key: 'connect'),
//       "Set-Cookie": CacheHelper.getData(key: 'connect'),
//     }).then((value) {
//       CacheHelper.setDouble(
//           key: 'point', value: value?.data["data"]["points"].toDouble());
//       emit(AppGetPointsState());
//     }).catchError((error) {
//       emit(AppGetPointsErrorState());
//     });
//   }

//   bool isScure = true;
//   var icon = Icons.visibility;
//   suffixicon() {
//     isScure = !isScure;
//     icon = isScure ? Icons.visibility : Icons.visibility_off;
//     emit(AppTextisScureState());
//   }

//   removeCache() {
//     CacheHelper.removeData();
//   }
// }
