import 'dart:convert';

import 'package:obadi/models/barcode_model.dart';
import 'package:obadi/models/pre_scans_model.dart';
import 'package:obadi/models/transitions_model.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../imports.dart';
import 'package:location/location.dart' as location;
part 'app_state.dart';

class AppCubit extends Cubit<AppState> {
  AppCubit() : super(AppInitial());

  AppCubit get(context) => BlocProvider.of(context);

  QRViewController? qrViewController;
  Barcode? result;

  scanningData(p0) {
    qrViewController = p0;
    qrViewController?.scannedDataStream.listen((event) {
      result = event;
      qrViewController?.stopCamera();
      emit(PostScanQrCodeLoadingState());
      postQrcode("${result?.code}");
    });
  }

  initCache() async {
    await CacheHelper.init().then((value) {
      CacheHelper.getData(key: 'locale');
      locale = Locale(CacheHelper.getData(key: 'locale') ?? 'ar');
      getPoints();
      emit(AppInitCacheState());
    });
  }

  String lang = CacheHelper.getData(key: 'locale') ?? 'ar';
  Locale locale = Locale(CacheHelper.getData(key: 'locale') ?? 'ar');

  changeLocale(String value) {
    locale = Locale(value);
    CacheHelper.setString(key: 'locale', value: value);

    emit(AppChangeLocaleState());
  }

  askPermission() async {
    await openAppSettings();
    emit(AppAskPermissionState());
  }

  location.Location loc = location.Location();
  dynamic permissionGranted;
  bool? serviceEnabled;

  Future checkPermission(amount, context, cubit) async {
    permissionGranted = await loc.hasPermission();
    serviceEnabled = await loc.serviceEnabled();
    startTransition(amount, context, cubit);
  }

  String locate = '';
  Future<String> enableService() async {
    if (!serviceEnabled!) {
      await loc.requestService().then((value) {
        if (value) {
          getLocation().then((value) {
            locate = "${value.longitude},${value.latitude}";
          });
        } else {
          showSnackBarr(msg: 'You should enable the location');
        }
      });
    } else {
      getLocation().then((value) {
        locate = "${value.latitude},${value.longitude}";
      });
    }
    return locate;
  }

  Future<location.LocationData> getLocation() async {
    return await loc.getLocation().then((value) {
      emit(AppGetLoctionState());
      return value;
    });
  }

  final Uri uri = Uri(scheme: 'tel', path: '+963969333060');
  luanchUri() async {
    await launchUrl(uri);
    emit(AppLaunchUrlState());
  }

//----------------------------------------------------------------------------
  PreScansModel? preScans;
  Future<void> getPreScans() async {
    emit(GetPreScansLoadingState());
    try {
      Response? response = await DioHelper.sendAuthenticatedRequest(
          "barcodes",
          Options(method: "GET", headers: {
            "Authorization": "Bearer ${CacheHelper.getData(key: "token")}"
          }));
      print(response?.data);
      preScans = PreScansModel.fromJson(response?.data);
      emit(GetPreScansSuccessState(preScans!));
    } on DioError catch (e) {
      print(e.toString());
      emit(GetPreScansErrorState());
    }
  }

  // PreScansModel? preScansModel;
  // getPreScans() {
  //   emit(AppLoadingState());
  //   String token = CacheHelper.getData(key: "token");
  //   DioHelper.getData('barcodes/consume',
  //       headres: {"Authorization": "Bearer $token"}).then((value) {
  //     preScansModel = PreScansModel.fromJson(value?.data);
  //     emit(AppGetPreScansState());
  //   }).catchError((error) {
  //     print(error.toString());
  //     emit(AppGetPreScansErrorState());
  //   });
  // }
  TransitionsModel? transitions;
  Future<void> getTransitions() async {
    emit(GetTransitionsLoadingState());
    try {
      Response? response = await DioHelper.sendAuthenticatedRequest(
          "transitions",
          Options(method: "GET", headers: {
            "Authorization": "Bearer ${CacheHelper.getData(key: "token")}"
          }));
      print(response?.data);
      transitions = TransitionsModel.fromJson(response?.data);
      emit(GetTransitionsSuccessState(transitions!));
    } on DioError catch (e) {
      emit(GetTransitionsErrorState());
    }
  }

  // TransitionsModel? transitionsModel;
  // getTransitions() {
  //   emit(AppLoadingState());
  //   DioHelper.getData('client/transitions', headres: {
  //     "Keep-Alive": "timeout=5, max=100",
  //     "Cookie": CacheHelper.getData(key: 'connect'),
  //     "Set-Cookie": CacheHelper.getData(key: 'connect'),
  //   }).then((value) {
  //     transitionsModel = TransitionsModel.fromJson(value?.data);
  //     emit(AppGetTransitionsState());
  //   }).catchError((error) {
  //     emit(AppGetTransitionsErrorState());
  //   });
  // }
  Future<void> startTransition(int amount, BuildContext context, cubit) async {
    emit(StartTransaitionLoadingState());
    loc.requestPermission().then((value) async {
      value.toString() == "PermissionStatus.denied" ||
              value.toString() == "PermissionStatus.deniedForever"
          ? openPermissionSettings(
              context, cubit, AppLocalizations.of(context)?.access_location)
          : await loc.requestService().then((value) {
              if (value) {
                getLocation().then((value) async {
                  locate = "${value.latitude},${value.longitude}";
                  try {
                    Response? response =
                        await DioHelper.sendAuthenticatedRequest(
                                "transitions/start",
                                Options(method: "POST", headers: {
                                  "Authorization":
                                      "Bearer ${CacheHelper.getData(key: "token")}"
                                }),
                                data: {"location": locate, "amount": amount})
                            .then((value) {
                      getPoints();
                      emit(StartTransitionSuccessState());
                    }).catchError((error) {
                      print(error.response?.data);
                      emit(StartTransaitionErrorState(
                          statusCode: error.response.statusCode ?? 0,
                          message: error.response.data["message"] ?? ""));
                    });
                  } on DioError catch (e) {}
                });
              } else {
                showSnackBarr(msg: 'You should enable the location');
              }
            });
    });
  }

  int? points;
  Future<void> postQrcode(String code) async {
    emit(PostScanQrCodeLoadingState());
    //try {
    //Response? response = await
    DioHelper.sendAuthenticatedRequest(
        "barcodes/consume",
        Options(method: "POST", headers: {
          "Authorization": "Bearer ${CacheHelper.getData(key: "token")}"
        }),
        data: {"code": code}).then((value) {
      //  print(value?.data);
      points = int.parse("${value?.data["points"]}");
      getPoints();
      emit(PostScanQrCodeSuccessState());
    }).catchError((e) {
      emit(PostScanQrCodeErrorState(statusCode: e.response.statusCode ?? 0));
    });

    // } on DioError catch (e) {
    //   emit(PostScanQrCodeErrorState(
    //       e.response?.data["message"], e.response?.data["statusCode"]));
    // }
  }

  Future<void> getPoints() async {
    try {
      Response? response = await DioHelper.sendAuthenticatedRequest(
          "transitions/points",
          Options(method: "GET", headers: {
            "Authorization": "Bearer ${CacheHelper.getData(key: "token")}"
          }));

      print(response?.data);
      CacheHelper.setDouble(
          key: "point",
          value: double.parse("${response?.data["data"]["points"]}"));
      emit(AppGetPointsState());
    } on DioError catch (e) {
      emit(AppGetPointsErrorState());
    }
  }
  // BarcodeModel? barcodeModel;
  // postQrcode(result) {
  //   emit(AppLoadingState());
  //   DioHelper.postData('client/barcode/consume', {
  //     "code": "$result"
  //   }, headres: {
  //     "Cookie": CacheHelper.getData(key: 'connect'),
  //     "Set-Cookie": CacheHelper.getData(key: 'connect'),
  //   }).then((value) {
  //     barcodeModel = BarcodeModel.fromJson(value?.data);
  //     startTransform(barcodeModel?.id);
  //     emit(AppPostScanQrCodeState());
  //   }).catchError((error) {
  //     emit(AppPostScanQrCodeErrorState());
  //   });
  // }

  removeCache() {
    CacheHelper.removeData();
  }
}
