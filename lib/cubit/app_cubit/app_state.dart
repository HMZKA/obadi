part of 'app_cubit.dart';

abstract class AppState {}

class AppInitial extends AppState {}

class AppScanQrCodeState extends AppState {}

class AppChangeLocaleState extends AppState {}

class AppInitCacheState extends AppState {}

class AppLaunchUrlState extends AppState {}

class AppPermissionState extends AppState {}

class AppAskPermissionState extends AppState {}

class AppPermissionLocationState extends AppState {
  String status;
  AppPermissionLocationState(this.status);
}

class GetPreScansLoadingState extends AppState {}

class GetPreScansSuccessState extends AppState {
  final PreScansModel preScansModel;

  GetPreScansSuccessState(this.preScansModel);
}

class GetPreScansErrorState extends AppState {}

class RemoveCacheState extends AppState {}

class GetTransitionsLoadingState extends AppState {}

class GetTransitionsSuccessState extends AppState {
  final TransitionsModel transitions;

  GetTransitionsSuccessState(this.transitions);
}

class GetTransitionsErrorState extends AppState {}

class PostScanQrCodeLoadingState extends AppState {}

class PostScanQrCodeSuccessState extends AppState {
  // final String message;
  // final String agent;
  // final String amount;

  // PostScanQrCodeSuccessState(this.message, this.agent, this.amount);
}

class PostScanQrCodeErrorState extends AppState {
  int statusCode;
  PostScanQrCodeErrorState({required this.statusCode});
}

class PostTransformsState extends AppState {
  String value;
  PostTransformsState(this.value);
}

class AppPostTransformsErrorState extends AppState {}

class AppGetPointsState extends AppState {}

class AppGetPointsErrorState extends AppState {}

class AppTextisScureState extends AppState {}

class StartTransaitionLoadingState extends AppState {}

class StartTransitionSuccessState extends AppState {}

class StartTransaitionErrorState extends AppState {
  final String message;
  final int statusCode;
  StartTransaitionErrorState({required this.message, required this.statusCode});
}

class AppGetLoctionState extends AppState {}
