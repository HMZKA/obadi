part of 'auth_cubit.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class AppGetLoctionState extends AuthState {}

class AppLoginLoadingState extends AuthState {}

class AppLoginSuccessState extends AuthState {
  bool isVerify;
  AppLoginSuccessState({required this.isVerify});
}

class AppLoginErrorState extends AuthState {
  int statusCode;
  AppLoginErrorState({required this.statusCode});
}

class AppRegisterLoadingState extends AuthState {}

class AppRegisterSuccessState extends AuthState {}

class AppRegisterErrorState extends AuthState {
  int? statusCode;
  AppRegisterErrorState({required this.statusCode});
}

class AppRequestLoadingState extends AuthState {}

class AppRequestVerifyUserState extends AuthState {}

class AppRequestVerifyUserErrorState extends AuthState {}

class AppVerifyLoadingState extends AuthState {}

class AppVerifyState extends AuthState {}

class AppVerifyErrorState extends AuthState {
  int? statusCode;
  AppVerifyErrorState({required this.statusCode});
}

class AppRequestResetLoadingState extends AuthState {}

class AppRequestResetCodeState extends AuthState {}

class AppRequestResetCodeErrorState extends AuthState {
  int? statusCode;
  AppRequestResetCodeErrorState({required this.statusCode});
}

class AppPostReserLoadingState extends AuthState {}

class AppPostResetCodeState extends AuthState {}

class AppPostResetCodeErrorState extends AuthState {
  int? statusCode;
  AppPostResetCodeErrorState({required this.statusCode});
}

class AppChangePasswordLodingState extends AuthState {}

class AppChangePasswordState extends AuthState {
  String result;
  AppChangePasswordState(this.result);
}

class AppChangePasswordErrorState extends AuthState {}

class AppTextisScureState extends AuthState {}
