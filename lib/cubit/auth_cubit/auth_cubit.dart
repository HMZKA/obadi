import 'package:bloc/bloc.dart';
import 'package:jwt_decoder/jwt_decoder.dart';
import 'package:meta/meta.dart';

import '../../imports.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());
  AuthCubit get(context) => BlocProvider.of(context);

  UserModel? userModel;
  login(numberPhone, password) {
    emit(AppLoginLoadingState());
    DioHelper.postData(
            "auth/login", {"mobile": numberPhone, "password": password})
        .then((value) {
      print(value?.data);
      userModel = UserModel.fromJson(value?.data);
      Map<String, dynamic> decodedToken =
          JwtDecoder.decode("${userModel?.token}");
      CacheHelper.setString(key: "token", value: "${userModel?.token}");
      CacheHelper.setBool(key: "verified", value: decodedToken["is_verified"]);
      CacheHelper.setString(key: "name", value: "${userModel?.user?.name}");
      CacheHelper.setString(key: "mobile", value: "${userModel?.user?.mobile}");
      CacheHelper.setString(key: "password", value: password);
      CacheHelper.setDouble(
          key: "point", value: double.parse("${userModel?.user?.points}"));
      emit(
          AppLoginSuccessState(isVerify: CacheHelper.getData(key: "verified")));
    }).catchError((error) {
      print(error.toString());

      emit(AppLoginErrorState(statusCode: error.response?.statusCode ?? 0));
    });
  }

  User? user;

  register(name, numberPhone, password) {
    emit(AppRegisterLoadingState());
    DioHelper.postData('auth/signup', {
      "name": "$name",
      "mobile": "$numberPhone",
      "password": "$password",
    }).then((value) {
      user = User.fromJson(value?.data);
      CacheHelper.setString(key: 'name', value: name);
      CacheHelper.setDouble(key: 'point', value: 0);
      CacheHelper.setString(key: "password", value: password);
      CacheHelper.setString(key: 'mobile', value: user?.mobile as String);
      emit(AppRegisterSuccessState());
    }).catchError((error) {
      print(error.toString());
      emit(AppRegisterErrorState(statusCode: error.response?.statusCode ?? 0));
    });
  }

  requestCode() {
    emit(AppRequestLoadingState());
    DioHelper.postData(
      'auth/mobile/request/verify',
      {"mobile": CacheHelper.getData(key: "mobile")},
    ).then((value) {
      emit(AppRequestVerifyUserState());
    }).catchError((error) {
      emit(AppRequestVerifyUserErrorState());
    });
  }

  verifyUser(code) {
    emit(AppVerifyLoadingState());
    DioHelper.postData(
      'auth/mobile/verify',
      {"mobile": CacheHelper.getData(key: "mobile"), "code": code},
    ).then((value) {
      emit(AppVerifyState());
    }).catchError((error) {
      print(error.response.data.toString());
      emit(AppVerifyErrorState(statusCode: error.response.statusCode ?? 0));
    });
  }

  requestResetCode(String number) {
    emit(AppRequestResetLoadingState());
    DioHelper.postData('auth/password/request/reset', {"mobile": number})
        .then((value) {
      emit(AppRequestResetCodeState());
    }).catchError((error) {
      print(error.toString());
      print(error.response.data.toString());
      emit(AppRequestResetCodeErrorState(
          statusCode: error.response.statusCode ?? 0));
    });
  }

  postResetPassword(code, password) {
    emit(AppPostReserLoadingState());
    DioHelper.postData('auth/password/reset', {
      "mobile": CacheHelper.getData(key: 'mobile'),
      "code": code,
      "newPassword": password
    }).then((value) {
      emit(AppPostResetCodeState());
    }).catchError((error) {
      print(error.response.data.toString());
      emit(AppPostResetCodeErrorState(
          statusCode: error.response.statusCode ?? 0));
    });
  }

  var newPasswordForm = GlobalKey<FormState>();
  var newPasswordController = TextEditingController();
  var repeatController = TextEditingController();
  var otpKey = GlobalKey<FormState>();
  var formRegisterkey = GlobalKey<FormState>();

  var codeController = TextEditingController();
  var phoneController = TextEditingController();
  var formkey = GlobalKey<FormState>();
  var loginpasswordController = TextEditingController();
  bool isScure = true;

  var icon = Icons.visibility;
  suffixicon() {
    isScure = !isScure;
    icon = isScure ? Icons.visibility : Icons.visibility_off;
    emit(AppTextisScureState());
  }
}
