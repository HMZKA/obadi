import '/imports.dart';

part 'internet_state.dart';

class InternetCubit extends Cubit<InternetState> {
  // Checks for Internet Connectivity and if the vpn is enabled
  bool isConnected = true;

  final Connectivity connectivity;
  late StreamSubscription connectivityStream;
  InternetCubit({required this.connectivity}) : super(InternetLoading()) {
    connectivityStream = connectivity.onConnectivityChanged
        .listen((ConnectivityResult connectivityResult) {
      // Checking if there's no Internet Connection
      if (connectivityResult == ConnectivityResult.none) {
        isConnected = false;
        showSnackBarr(
          msg: "No Internet Connection",
        );
        emit(InternetDisconnected());
      } else {
        // Checking if there's a vpn running
        if (connectivityResult == ConnectivityResult.vpn) {
          isConnected = false;
          showSnackBarr(msg: "You can't use the app with vpn");
          emit(VpnConnected());
        }
      }
      if (isConnected == false) {
        if (connectivityResult == ConnectivityResult.mobile ||
            connectivityResult == ConnectivityResult.wifi) {
          isConnected = true;
          showSnackBarr(
              msg: "Internet Connected Successfully", color: Colors.green);
          emit(InternetConnected());
        }
      }
    });
  }

  @override
  Future<void> close() {
    connectivityStream.cancel();
    return super.close();
  }
}
