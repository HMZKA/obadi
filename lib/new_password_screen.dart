import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';

class PasswordScreen extends StatelessWidget {
  const PasswordScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        var cubit = AuthCubit().get(context);
        if (state is AppPostResetCodeErrorState) {
          state.statusCode == 0
              ? showSnackBarr(
                  msg: "${AppLocalizations.of(context)?.connection_failed}")
              : showSnackBarr(
                  msg:
                      "${AppLocalizations.of(context)?.incorrect_credentials}");
        } else if (state is AppPostResetCodeState) {
          cubit.codeController.text = "";
          cubit.repeatController.text = "";
          cubit.newPasswordController.text = "";
          CacheHelper.setBool(key: "verified", value: false);
          navigateRemove(context, LoginScreen());
        }
      },
      builder: (context, state) {
        var cubit = AuthCubit().get(context);
        return Scaffold(
          appBar:
              ObadiAppBar('${AppLocalizations.of(context)?.change_password}')
                  .build(context),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 50),
            child: Form(
              key: cubit.newPasswordForm,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 12.0),
                      child: Directionality(
                        textDirection: TextDirection.ltr,
                        child: Pinput(
                          onCompleted: (value) {},
                          controller: cubit.codeController,
                          length: 6,
                          focusedPinTheme: const PinTheme(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black26,
                                        blurRadius: 15,
                                        spreadRadius: 0,
                                        offset: Offset(0, 0))
                                  ]),
                              height: 60,
                              width: 56),
                        ),
                      ),
                    ),
                    textBox('${AppLocalizations.of(context)?.new_password}',
                        (String? value) {
                      if (value!.isEmpty) {
                        return '${AppLocalizations.of(context)?.you_should_enter_password}';
                      }
                      if (value.length < 8) {
                        return "${AppLocalizations.of(context)?.passwordLength}";
                      }
                    }, cubit.newPasswordController,
                        TextInputType.visiblePassword,
                        suffixIcon: IconButton(
                          icon: Icon(
                            cubit.icon,
                            color: mainColor,
                          ),
                          onPressed: () {
                            cubit.suffixicon();
                          },
                        ),
                        isScure: cubit.isScure),
                    const SizedBox(
                      height: 25,
                    ),
                    textBox('${AppLocalizations.of(context)?.repeat_password}',
                        (String? value) {
                      if (value!.isEmpty) {
                        return '${AppLocalizations.of(context)?.you_should_enter_password}';
                      }
                      if (value != cubit.newPasswordController.text) {
                        return '${AppLocalizations.of(context)?.it_not_the_same}';
                      }
                    }, cubit.repeatController, TextInputType.visiblePassword,
                        suffixIcon: IconButton(
                          icon: Icon(
                            cubit.icon,
                            color: mainColor,
                          ),
                          onPressed: () {
                            cubit.suffixicon();
                          },
                        ),
                        isScure: cubit.isScure),
                    const SizedBox(height: 20),
                    signButton(
                        context,
                        Center(
                            child: state is AppPostReserLoadingState
                                ? const CircularProgressIndicator(
                                    color: Colors.white,
                                  )
                                : Text(
                                    '${AppLocalizations.of(context)?.save}',
                                    style: const TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  )), () {
                      cubit.postResetPassword(cubit.codeController.text,
                          cubit.newPasswordController.text);
                    }, state)
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
