class TransitionsModel {
  List<Data>? data = [];

  TransitionsModel.fromJson(Map<String, dynamic> json) {
    json['data'].forEach((v) {
      data?.add(Data.fromJson(v));
    });
  }
}

class Data {
  int? id;
  int? isSent;
  int? isSucess;
  int? points;
  String? date;
  String? dateSent;

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];

    isSent = json['isSent'];
    isSucess = json['isSucess'];

    points = json['points'];
    date = json['sentAt'];
    dateSent = json['dateSent'];
  }
}
