class UserModel {
  String? token;
  User? user;
  UserModel.fromJson(Map<String, dynamic> json) {
    token = json["token"];
    user = User.fromJson(json["user"]);
  }
}

class User {
  String? name;
  String? mobile;
  int? points;
  User.fromJson(Map<String, dynamic> json) {
    name = json["name"];
    mobile = json["mobile"];
    points = json["points"];
  }
}
