import 'package:obadi/models/barcode_model.dart';

class PreScansModel {
  List<BarcodeModel>? data = [];

  PreScansModel.fromJson(List json) {
    json.forEach((v) {
      data?.add(BarcodeModel.fromJson(v));
    });
  }
}
