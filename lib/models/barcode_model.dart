class BarcodeModel {
  int? points;
  String? date;
  BarcodeModel.fromJson(Map<String, dynamic> json) {
    points = int.parse("${json['points']}");
    date = json['usedAt'];
  }
}
