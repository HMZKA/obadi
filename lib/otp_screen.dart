import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';
import 'new_password_screen.dart';

class OtpScreen extends StatelessWidget {
  OtpScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AppVerifyState) {
          CacheHelper.setBool(key: "verified", value: true);
          AuthCubit().get(context).login(CacheHelper.getData(key: "mobile"),
              CacheHelper.getData(key: "password"));
          navigateRemove(context, MainScreen());
        } else if (state is AppVerifyErrorState) {
          state.statusCode == 0
              ? showSnackBarr(
                  msg: "${AppLocalizations.of(context)?.connection_failed}")
              : showSnackBarr(
                  msg:
                      "${AppLocalizations.of(context)?.incorrect_credentials}");
        }
      },
      builder: (context, state) {
        var cubit = AuthCubit().get(context);
        return Scaffold(
          appBar: ObadiAppBar('').build(context),
          body: Padding(
            padding: const EdgeInsets.only(top: 40, right: 15, left: 15),
            child: Column(children: [
              Text(
                '${AppLocalizations.of(context)?.enter_the_code}',
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 30,
              ),
              Form(
                key: cubit.otpKey,
                child: Directionality(
                  textDirection: TextDirection.ltr,
                  child: Pinput(
                    onCompleted: (value) {
                      cubit.verifyUser(value);
                    },
                    length: 6,
                    focusedPinTheme: const PinTheme(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 15,
                                  spreadRadius: 0,
                                  offset: Offset(0, 0))
                            ]),
                        height: 60,
                        width: 56),
                  ),
                ),
              ),
              const SizedBox(
                height: 12,
              ),
              BlocBuilder<AuthCubit, AuthState>(
                builder: (context, state) {
                  return state is AppRequestLoadingState
                      ? Center(
                          child: CircularProgressIndicator(
                            color: mainColor,
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "${AppLocalizations.of(context)?.didnt_get_the_code}",
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            TextButton(
                              child: Text(
                                "${AppLocalizations.of(context)?.get_new}",
                                style: TextStyle(color: mainColor),
                              ),
                              onPressed: () {},
                            )
                          ],
                        );
                },
              )
            ]),
          ),
        );
      },
    );
  }
}
