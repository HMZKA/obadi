import 'imports.dart';

class ObadiAppBar {
  String title;
  ObadiAppBar(this.title);

  PreferredSizeWidget build(BuildContext context, {leading, actions}) {
    return AppBar(
      title: Text(title),
      centerTitle: true,
      elevation: 5,
      leading: leading,
      actions: actions,
      flexibleSpace: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [mainColor, const Color.fromARGB(255, 32, 32, 32)],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
      ),
    );
  }
}
