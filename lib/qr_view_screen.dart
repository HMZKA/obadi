import 'imports.dart';

class QrScreen extends StatelessWidget {
  const QrScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {
        state is PostScanQrCodeLoadingState
            ? {
                popNavigation(context),
              }
            : null;
      },
      builder: (context, state) {
        var cubit = AppCubit().get(context);
        var qrKey = GlobalKey();

        return Scaffold(
          appBar: ObadiAppBar('${AppLocalizations.of(context)?.qr_scanner}')
              .build(context),
          body: Stack(
            alignment: Alignment.topLeft,
            children: [
              Column(
                children: [
                  Expanded(
                    flex: 5,
                    child: QRView(
                      onPermissionSet: (p0, p1) {
                        if (!p1) {
                          popNavigation(context);
                          openPermissionSettings(context, cubit,
                              AppLocalizations.of(context)?.access_camera);
                        } else {
                          cubit.qrViewController?.resumeCamera();
                        }
                      },
                      overlay: QrScannerOverlayShape(
                          borderLength: 38,
                          borderRadius: 20,
                          cutOutSize: 300,
                          borderColor: mainColor),
                      key: qrKey,
                      onQRViewCreated: (p0) {
                        cubit.scanningData(p0);
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Center(
                      child: Text(
                        '${AppLocalizations.of(context)?.scan_the_code_to_win_with_us}',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: mainColor),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(mainColor)),
                      onPressed: () {
                        cubit.qrViewController?.flipCamera();
                      },
                      child: const Icon(Icons.flip_camera_ios),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(mainColor)),
                      child: const Icon(Icons.flash_on),
                      onPressed: () {
                        cubit.qrViewController?.toggleFlash();
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
