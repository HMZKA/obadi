import 'package:buildcondition/buildcondition.dart';

import 'imports.dart';

class PreviousScreen extends StatelessWidget {
  const PreviousScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {},
      builder: (context, state) {
        var cubit = AppCubit().get(context);
        return Scaffold(
            appBar:
                ObadiAppBar('${AppLocalizations.of(context)?.previous_scan}')
                    .build(context),
            body: BuildCondition(
              condition: cubit.preScans?.data != null,
              builder: (context) {
                return cubit.preScans?.data?.length != 0
                    ? ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        itemCount: cubit.preScans?.data?.length,
                        itemBuilder: (BuildContext context, int index) {
                          return buildPreScans(Icons.qr_code_2, context,
                              cubit.preScans?.data?[index]);
                        },
                      )
                    : Center(
                        child: Text(
                          '${AppLocalizations.of(context)?.no_prescan}',
                          style: TextStyle(
                              color: mainColor,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      );
              },
              fallback: (context) => Center(
                child: CircularProgressIndicator(color: mainColor),
              ),
            ));
      },
    );
  }
}
