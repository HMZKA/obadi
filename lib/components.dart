import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';
import 'my_dialoge.dart';

//widgets
buildDrawerItem(
    {required String title,
    required VoidCallback onTap,
    required IconData icon}) {
  return Material(
    color: Colors.transparent,
    child: ListTile(
      leading: Icon(icon),
      title: Text(title),
      style: ListTileStyle.drawer,
      iconColor: Colors.white,
      textColor: Colors.white,
      onTap: onTap,
    ),
  );
}

buildPreScans(icon, context, model, {bool winScreen = true}) {
  return Container(
    padding: const EdgeInsets.all(4),
    margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
              blurRadius: 5, offset: Offset(4.5, 2), color: Colors.black38)
        ],
        color: mainColor),
    child: Row(
      children: [
        Icon(
          icon,
          size: 80,
          color: Colors.white,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                winScreen
                    ? '${AppLocalizations.of(context)?.win_with_us} ${model.points} ${AppLocalizations.of(context)?.point}'
                    : '${AppLocalizations.of(context)?.transition_done} \n ${AppLocalizations.of(context)?.amount} ${model.points} ${AppLocalizations.of(context)?.point}',
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 6,
              ),
              Text('${model.date?.split("T").elementAt(0)}',
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500))
            ],
          ),
        )
      ],
    ),
  );
}

textBox(title, validator, controller, textInputtype,
    {bool? isScure, Widget? suffixIcon, onChange}) {
  return TextFormField(
      controller: controller,
      keyboardType: textInputtype,
      validator: validator,
      cursorColor: mainColor,
      onChanged: onChange,
      obscureText: isScure ?? false,
      decoration: InputDecoration(
        label: Text(
          title,
          style: TextStyle(color: mainColor),
        ),
        suffixIcon: suffixIcon,
        hintText: '$title',
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: mainColor,
            )),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: mainColor,
            )),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Colors.red,
          ),
        ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              color: mainColor,
            )),
      ));
}

showTransitionDialog(context, cubit, point) {
  showDialog(
      builder: (context) => BlocBuilder<AppCubit, AppState>(
            builder: (context, state) {
              return Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        '${AppLocalizations.of(context)?.transition}',
                        style: TextStyle(
                            color: mainColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height / 10,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: amounts.length + 1,
                        shrinkWrap: true,
                        cacheExtent: 200,
                        separatorBuilder: (BuildContext context, int index) {
                          return Center(
                            child: TextButton(
                              onPressed: () {
                                popNavigation(context);
                                if (point < amounts['${index + 1}']) {
                                  showSnackBarr(
                                      msg:
                                          '${AppLocalizations.of(context)?.no_enough_points}');
                                } else {
                                  cubit.checkPermission(
                                      amounts['${index + 1}'], context, cubit);
                                }
                              },
                              style: ButtonStyle(
                                  overlayColor: MaterialStateProperty.all(
                                      mainColor.withAlpha(50)),
                                  side: MaterialStateProperty.all(
                                      BorderSide(color: mainColor))),
                              child: Text(
                                '${amounts["${index + 1}"]}',
                                style:
                                    TextStyle(fontSize: 16, color: mainColor),
                              ),
                            ),
                          );
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return const SizedBox(
                            width: 15,
                          );
                        },
                      ),
                    ),
                    Text(
                      '${AppLocalizations.of(context)?.choose_amount}',
                      style: TextStyle(color: mainColor, fontSize: 16),
                    )
                  ],
                ),
              );
            },
          ),
      context: context);
}

showOtpCodedialoge(context, {String? number, required CodeType codeType}) {
  showDialog(
      builder: (context) => AlertDialog(
            content: Text('${AppLocalizations.of(context)?.do_you_have_code}'),
            actions: [
              TextButton(
                onPressed: () {
                  popNavigation(context);
                  pushNavigation(context, OtpScreen());
                },
                child: Text(
                  '${AppLocalizations.of(context)?.yes}',
                  style: TextStyle(color: mainColor),
                ),
              ),
              TextButton(
                child: Text(
                  '${AppLocalizations.of(context)?.get_new}',
                  style: TextStyle(color: mainColor),
                ),
                onPressed: () {
                  popNavigation(context);
                  AuthCubit().get(context).requestResetCode(number.toString());
                  pushNavigation(context, OtpScreen());
                },
              )
            ],
          ),
      context: context);
}

signButton(context, Widget child, onTap, state) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: mainColor, borderRadius: BorderRadius.circular(20)),
        width: MediaQuery.of(context).size.width / 1.5,
        child: child),
  );
}

openPermissionSettings(context, cubit, content) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(content: Text('$content'), actions: [
      TextButton(
        onPressed: () {
          popNavigation(context);
          cubit.askPermission();
        },
        child: Text(
          '${AppLocalizations.of(context)?.open_settings}',
          style: TextStyle(color: mainColor),
        ),
      ),
      TextButton(
        child: Text(
          '${AppLocalizations.of(context)?.close}',
          style: TextStyle(color: mainColor),
        ),
        onPressed: () {
          popNavigation(context);
        },
      )
    ]),
  );
}

buildProfileItem(context, title, content, icon) {
  return Card(
    margin: const EdgeInsets.symmetric(vertical: 10),
    elevation: 10,
    shadowColor: Colors.black38,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    child: Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          Text(
            '$title:',
            style: TextStyle(
                color: mainColor, fontSize: 16, fontWeight: FontWeight.w600),
          ),
          const Expanded(
            flex: 1,
            child: SizedBox(),
          ),
          Text(
            '$content',
            style: TextStyle(
                color: mainColor, fontSize: 16, fontWeight: FontWeight.w600),
          ),
          const Expanded(
            flex: 2,
            child: SizedBox(),
          ),
          Icon(
            icon,
            color: mainColor,
          ),
        ],
      ),
    ),
  );
}

// showErrorDialoge(context, String errorMessage) {
//   showDialog(
//     context: context,
//     builder: (context) => AlertDialog(
//       content: Text(errorMessage),
//       actions: [
//         TextButton(
//             onPressed: () {
//               popNavigation(context);
//             },
//             child: Text(
//               "Ok",
//               style: TextStyle(color: Colors.black),
//             ))
//       ],
//     ),
//   );
// }

showWinDialoge(
  context,
) {
  showDialog(context: context, builder: (context) => MyDialoge());
}

// When Calling the Function set the value of color to null so you give the snackbar the default color
void showSnackBarr({
  required String msg,
  Color? color,
}) {
  final SnackBar snackBar = SnackBar(
    content: Text(
      msg,
      textAlign: TextAlign.center,
      style: const TextStyle(fontSize: 18),
    ),
    backgroundColor: color,
  );
  snackbarKey.currentState?.showSnackBar(snackBar);
}

showVerifyDialoge(context) {
  String mobile = CacheHelper.getData(key: 'mobile');
  showDialog(
    builder: (context) => AlertDialog(
        content: Text('؟ $mobile هل أنت متأكد ان رقمك هو'),
        actions: [
          TextButton(
            onPressed: () {
              popNavigation(context);
              pushNavigation(context, OtpScreen());
            },
            child: Text(
              'نعم',
              style: TextStyle(color: mainColor),
            ),
          ),
          TextButton(
            child: Text(
              'لا',
              style: TextStyle(color: mainColor),
            ),
            onPressed: () {
              popNavigation(context);
            },
          )
        ]),
    context: context,
  );
}

//----------------------------------------------------------------
// functions
pushNavigation(context, widget) {
  // Navigator.push(
  //   context,
  //   PageRouteBuilder(
  //     transitionsBuilder: (context, animation, secondaryAnimation, child) {
  //       return ScaleTransition(
  //         alignment: Alignment.bottomCenter,
  //         scale: Tween<double>(begin: 0.1, end: 1).animate(
  //           CurvedAnimation(
  //             parent: animation,
  //             curve: Curves.easeOut,
  //           ),
  //         ),
  //         child: child,
  //       );
  //     },
  //     transitionDuration: const Duration(milliseconds: 300),
  //     pageBuilder: (BuildContext context, Animation<double> animation,
  //         Animation<double> secondaryAnimation) {
  //       return widget;
  //     },
  //   ),
  // );
  Navigator.of(context).push(MaterialPageRoute(
    builder: (context) => widget,
  ));
}

popNavigation(context) {
  Navigator.of(context).pop();
}

navigateRemove(context, widget) {
  Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(
        builder: (context) => widget,
      ),
      (route) => false);
}
