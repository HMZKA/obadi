import 'package:lottie/lottie.dart';
import 'package:phone_number/phone_number.dart';

import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';

//ydtyddrrghydeggh
// ignore: must_be_immutable
class RegisterScreen extends StatefulWidget {
  RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  var formRegisterkey = GlobalKey<FormState>();
  var nameController = TextEditingController();
  var passwordController = TextEditingController();
  var rePasswordController = TextEditingController();
  var registerPhoneController = TextEditingController();
  bool? isSyrianNumber;
  Future validatePhoneNumber(value) async {
    await PhoneNumberUtil().validate("$value", regionCode: 'SY').then((value) {
      setState(() {
        isSyrianNumber = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is AppRegisterSuccessState) {
          var cubit = AuthCubit().get(context);
          nameController.text = "";
          registerPhoneController.text = "";
          rePasswordController.text = "";
          passwordController.text = "";
          Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => OtpScreen(),
              ));
          AuthCubit().get(context).requestCode();
        } else if (state is AppRegisterErrorState) {
          if (state.statusCode == 0) {
            showSnackBarr(
                msg: "${AppLocalizations.of(context)?.connection_failed}");
          } else if (state.statusCode == 409) {
            showSnackBarr(
                msg: "${AppLocalizations.of(context)?.duplicatedUser}");
          } else {
            showSnackBarr(
                msg:
                    "الرجاء ملئ الحقول بالمعلومات الصحيحة طول كلمة السر اقل من 8 ");
          }
        }
      },
      builder: (context, state) {
        var cubit = AuthCubit().get(context);
        return Scaffold(
          appBar: ObadiAppBar('${AppLocalizations.of(context)?.register}')
              .build(context,
                  leading: IconButton(
                      onPressed: () {
                        popNavigation(context);
                        nameController.text = "";
                        registerPhoneController.text = "";
                        rePasswordController.text = "";
                        passwordController.text = "";
                      },
                      icon: Icon(Icons.arrow_back))),
          body: Padding(
            padding: const EdgeInsets.all(12.0),
            child: SingleChildScrollView(
              child: Form(
                key: formRegisterkey,
                child: Column(children: [
                  Lottie.asset('assets/images/cup.json',
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 5),
                  textBox(AppLocalizations.of(context)?.name, (String? value) {
                    if (value!.isEmpty) {
                      return '${AppLocalizations.of(context)?.you_should_enter_your_name}';
                    }
                  }, nameController, TextInputType.name),
                  const SizedBox(
                    height: 20,
                  ),
                  textBox(AppLocalizations.of(context)?.phone_number,
                      onChange: (p0) {
                    if (p0.length > 4) {
                      validatePhoneNumber(p0);
                    }
                  }, (String? value) {
                    if (value!.isEmpty) {
                      return '${AppLocalizations.of(context)?.you_should_enter_a_phone_number}';
                    } else if (!isSyrianNumber!) {
                      return "${AppLocalizations.of(context)?.syrianNumber}";
                    }
                    final firstnumber = value[0];
                    if (firstnumber == "0") {
                      value = "963${value.substring(1)}".trim();
                    }
                  }, registerPhoneController, TextInputType.phone),
                  const SizedBox(
                    height: 20,
                  ),
                  textBox(AppLocalizations.of(context)?.password,
                      (String? value) {
                    if (value!.isEmpty) {
                      return '${AppLocalizations.of(context)?.you_should_enter_password}';
                    } else if (value.length < 8) {
                      return "${AppLocalizations.of(context)?.passwordLength}";
                    }
                  }, passwordController, TextInputType.visiblePassword,
                      suffixIcon: IconButton(
                        icon: Icon(
                          cubit.icon,
                          color: mainColor,
                        ),
                        onPressed: () {
                          cubit.suffixicon();
                        },
                      ),
                      isScure: cubit.isScure),
                  const SizedBox(
                    height: 20,
                  ),
                  textBox(AppLocalizations.of(context)?.repeat_password,
                      (String? value) {
                    if (value!.isEmpty) {
                      return '${AppLocalizations.of(context)?.repeat_password}';
                    }
                    if (value != passwordController.text) {
                      return '${AppLocalizations.of(context)?.it_not_the_same}';
                    }
                  }, rePasswordController, TextInputType.visiblePassword,
                      suffixIcon: IconButton(
                        icon: Icon(
                          cubit.icon,
                          color: mainColor,
                        ),
                        onPressed: () {
                          cubit.suffixicon();
                        },
                      ),
                      isScure: cubit.isScure),
                  const SizedBox(
                    height: 20,
                  ),
                  signButton(
                      context,
                      Center(
                          child: state is AppRegisterLoadingState
                              ? const CircularProgressIndicator(
                                  color: Colors.white,
                                )
                              : Text(
                                  '${AppLocalizations.of(context)?.register}',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                )), () {
                    if (registerPhoneController.text.length == 10 ||
                        registerPhoneController.text.length == 12) {
                      final firstnumber = registerPhoneController.text[0];
                      if (firstnumber == "0") {
                        registerPhoneController.text =
                            "963${registerPhoneController.text.substring(1)}"
                                .trim();
                      }
                      cubit.register(
                          nameController.text,
                          registerPhoneController.text,
                          passwordController.text);
                    } else {
                      showSnackBarr(
                          msg: "${AppLocalizations.of(context)?.syrianNumber}");
                    }
                  }, state)
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
