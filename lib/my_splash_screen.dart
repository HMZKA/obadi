import 'imports.dart';

class MySplashScreen extends StatefulWidget {
  const MySplashScreen({super.key});

  @override
  State<MySplashScreen> createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen> {
  @override
  void initState() {
    initCache();
    super.initState();
  }

  initCache() async {
    await CacheHelper.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: SizedBox(
        width: 250,
        child: Stack(
          alignment: Alignment.center,
          children: [
            SizedBox(width: 250, child: Image.asset('assets/images/logo.png')),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                height: 130,
                width: 75,
                color: Colors.white,
              ),
            ),
            TweenAnimationBuilder(
                onEnd: () {
                  var verify = CacheHelper.getData(key: 'verified') ?? false;
                  if (verify == false) {
                    navigateRemove(context, LoginScreen());
                  } else {
                    navigateRemove(context, const MainScreen());
                  }
                },
                duration: const Duration(seconds: 5),
                tween: Tween(
                    begin: Alignment.centerRight, end: Alignment.centerLeft),
                builder: (BuildContext context, dynamic align, Widget? child) {
                  return Align(
                      alignment: Alignment.centerLeft,
                      child: TweenAnimationBuilder(
                        duration: const Duration(seconds: 4),
                        tween: Tween<double>(begin: 250, end: 75),
                        builder: (BuildContext context, dynamic value,
                            Widget? child) {
                          return Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(colors: [
                                Colors.white,
                                Colors.white.withOpacity(0.9)
                              ], stops: const [
                                0.85,
                                0.15
                              ])),
                              width: value,
                              height: 129.5,
                              child: Align(
                                  alignment: align,
                                  child: Image.asset(
                                      'assets/images/ic_launcher.png')));
                        },
                      ));
                })
          ],
        ),
      )),
    );
  }
}
