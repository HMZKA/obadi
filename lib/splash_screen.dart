import 'package:video_player/video_player.dart';

import 'imports.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  VideoPlayerController? controller;
  late Future<void> initialize;
  @override
  void initState() {
    controller = VideoPlayerController.asset('assets/images/splash.mp4');
    initialize = controller!.initialize();
    controller?.play();
    controller?.addListener(() {
      if (!(controller!.value.isPlaying)) {
        var verify = CacheHelper.getData(key: 'verified') ?? false;
        if (verify == false) {
          navigateRemove(context, LoginScreen());
        } else {
          navigateRemove(context, const MainScreen());
        }
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          colors: [
            Color.fromARGB(255, 87, 52, 0),
            Colors.black,
          ],
        )),
        child: Center(
          child: FutureBuilder(
            future: initialize,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return AspectRatio(
                  aspectRatio: controller!.value.aspectRatio,
                  child: VideoPlayer(controller!),
                );
              } else {
                return CircularProgressIndicator(
                  color: mainColor,
                );
              }
            },
          ),
        ),
      ),
    );
  }
}
