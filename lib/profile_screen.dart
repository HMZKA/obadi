import 'package:obadi/new_password_screen.dart';

import 'cubit/auth_cubit/auth_cubit.dart';
import 'imports.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthCubit, AuthState>(
      listener: (context, state) {},
      builder: (context, state) {
        var code = CacheHelper.getData(key: 'code') ?? '';
        var cubit = AuthCubit().get(context);
        String mobile = CacheHelper.getData(key: 'mobile') ?? '';
        return Scaffold(
          appBar: ObadiAppBar('${AppLocalizations.of(context)?.profile}')
              .build(context),
          body: SizedBox(
            width: double.infinity,
            child: Card(
              margin: const EdgeInsets.all(12.0),
              elevation: 20,
              shadowColor: Colors.black38,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25)),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(children: [
                  Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      //  border: Border.all(color: mainColor, width: 1.7),
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      'assets/images/ic_launcher.png',
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 6,
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  buildProfileItem(context, AppLocalizations.of(context)?.name,
                      "${CacheHelper.getData(key: 'name')}", Icons.person),
                  buildProfileItem(
                      context,
                      AppLocalizations.of(context)?.phone_number,
                      "${CacheHelper.getData(key: 'mobile')}",
                      Icons.call_end_rounded),
                  const SizedBox(height: 10),
                  signButton(
                      context,
                      Center(
                        child: Text(
                          "${AppLocalizations.of(context)?.change_password}",
                          style: TextStyle(color: Colors.white),
                        ),
                      ), () {
                    pushNavigation(context, PasswordScreen());
                    cubit.requestResetCode(mobile);
                  }, state)
                ]),
              ),
            ),
          ),
        );
      },
    );
  }
}
