import 'imports.dart';

class MyDialoge extends StatelessWidget {
  MyDialoge({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppCubit, AppState>(
      builder: (context, state) {
        return AlertDialog(
          insetPadding: EdgeInsets.zero,
          title: state is! PostScanQrCodeLoadingState
              ? Text(
                  "الحكي مع K.F.C طعمه غير",
                  textAlign: TextAlign.center,
                )
              : null,
          content: state is! PostScanQrCodeLoadingState
              ? Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.asset("assets/images/kfc.png"),
                    Text(
                        " لقد ربحت ${AppCubit().get(context).points} نقطة من شركة K.F.C")
                  ],
                )
              : CircularProgressIndicator(color: mainColor),
          actions: state is! PostScanQrCodeLoadingState
              ? [
                  TextButton(
                      onPressed: () {
                        popNavigation(context);
                      },
                      child: Text(
                        "حسناََ",
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      )),
                  TextButton(
                    child: Text(
                      "قرمش واحكي",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    onPressed: () {
                      popNavigation(context);
                      showTransitionDialog(context, AppCubit().get(context),
                          CacheHelper.getData(key: "point"));
                    },
                  )
                ]
              : [],
        );
      },
    );
  }
}
