import '/components.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

import 'package:flutter/material.dart';
import 'app_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localization.dart';
import 'cubit/app_cubit/app_cubit.dart';

class WebMobileScanner extends StatelessWidget {
  const WebMobileScanner({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {
        state is PostScanQrCodeLoadingState ? popNavigation(context) : null;
      },
      builder: (context, state) {
        var scannerController = MobileScannerController();
        var cubit = AppCubit().get(context);

        return Scaffold(
            appBar: ObadiAppBar('${AppLocalizations.of(context)?.qr_scanner}')
                .build(context, actions: [
              IconButton(
                icon: const Icon(Icons.flash_on),
                onPressed: () {
                  scannerController.toggleTorch();
                },
              )
            ]),
            body: MobileScanner(
              controller: scannerController,
              onDetect: (capture) {
                scannerController.stop();
                final List<Barcode> barcodes = capture.barcodes;
                cubit.postQrcode("${barcodes.first.rawValue}");
              },
            ));
      },
    );
  }
}
