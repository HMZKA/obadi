import 'dart:io';
import 'package:flutter/services.dart';

import 'package:obadi/my_splash_screen.dart';
import 'package:obadi/splash_screen.dart';
import 'cubit/auth_cubit/auth_cubit.dart';
import 'cubit/bloc_ob.dart';
import 'imports.dart';
import 'new_password_screen.dart';

void main() {
  runApp(MyApp(
    connectivity: Connectivity(),
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  DioHelper.init();
  HttpOverrides.global = MyHttpOverrides();
  Bloc.observer = MyBlocObserver();
}

class MyApp extends StatelessWidget {
  final Connectivity connectivity;
  const MyApp({super.key, required this.connectivity});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => AppCubit()..initCache()),
        BlocProvider(
          create: (context) => AuthCubit(),
        ),
        BlocProvider(
          create: (context) => InternetCubit(connectivity: connectivity),
        ),
      ],
      child: BlocConsumer<AppCubit, AppState>(
        listener: (context, state) {},
        builder: (context, state) {
          return MaterialApp(
            title: '3obadi',
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            scaffoldMessengerKey: snackbarKey,
            locale: AppCubit().get(context).locale,
            debugShowCheckedModeBanner: false,
            home: BlocBuilder<InternetCubit, InternetState>(
              builder: (context, state) {
                return const MySplashScreen();
              },
            ),
          );
        },
      ),
    );
  }
}
