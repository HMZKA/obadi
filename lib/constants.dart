import 'imports.dart';

// colors

var mainColor = const Color.fromARGB(255, 167, 100, 0);
final GlobalKey<ScaffoldMessengerState> snackbarKey =
    GlobalKey<ScaffoldMessengerState>();

Map<String, dynamic> amounts = {
  "1": 150,
  "2": 200,
  "3": 250,
  "4": 300,
  "5": 400,
  "6": 450,
  "7": 500,
  "8": 650,
  "9": 750,
  "10": 910,
  "11": 1000
};

enum CodeType {
  resetPassword,
  verifyAccount,
}
