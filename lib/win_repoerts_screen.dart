import 'package:buildcondition/buildcondition.dart';

import 'imports.dart';

class WinReportScreen extends StatelessWidget {
  const WinReportScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AppCubit, AppState>(
      listener: (context, state) {},
      builder: (context, state) {
        var cubit = AppCubit().get(context);
        return Scaffold(
            appBar: ObadiAppBar('${AppLocalizations.of(context)?.win_reports}')
                .build(context),
            body: BuildCondition(
              condition: cubit.transitions?.data != null,
              builder: (context) {
                return cubit.transitions?.data?.length != 0
                    ? ListView.builder(
                        physics: const BouncingScrollPhysics(),
                        itemCount: cubit.transitions?.data?.length,
                        itemBuilder: (BuildContext context, int index) {
                          return buildPreScans(Icons.call_received_outlined,
                              context, cubit.transitions?.data?[index],
                              winScreen: false);
                        },
                      )
                    : Center(
                        child: Text(
                          '${AppLocalizations.of(context)?.no_transition}',
                          style: TextStyle(
                              color: mainColor,
                              fontSize: 20,
                              fontWeight: FontWeight.w600),
                        ),
                      );
              },
              fallback: (context) => Center(
                  child: CircularProgressIndicator(
                color: mainColor,
              )),
            ));
      },
    );
  }
}
